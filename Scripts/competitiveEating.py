def competitiveEating(t, width, precision):
    return '{1:.{0}f}'.format(precision,round(t, precision)).center(width)